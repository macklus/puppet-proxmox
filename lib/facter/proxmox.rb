# proxmox.rb

pve = 'false'
pve_mayor = 'false'
pve_minor = 'false'

if File.exist?('/usr/bin/pveversion')
	out = IO.popen(['/usr/bin/pveversion', '-v']).read
	r = /pve-manager: (\d+).(\d+)-(\d+)/.match(out)
	pve = r[1] + "." + r[2] + "-" + r[3]
	pve_mayor = r[1]
	pve_minor = r[2]
end

Facter.add("pve") do
	setcode do
		pve
	end
end

Facter.add("pve_mayor") do
	setcode do
		pve_mayor
	end
end

Facter.add("pve_minor") do
	setcode do
		pve_minor
	end
end

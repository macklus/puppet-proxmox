#
#
#

define proxmox::acl($root='',$user='',$role='',$propagate=0) {

	# Security check 
	if( $root != '' )
	{
		$line = "acl:$propagate:$root:$user:$role:"

    	proxmox::functions::line { $line:
        	file => "/etc/pve/user.cfg",
	        ensure => present
    	}
	} else {
		notify{"ERROR: root argument are required on proxmox::acl!":}
	}
}

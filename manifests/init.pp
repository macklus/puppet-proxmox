#
# Proxmox puppet module
# Define proxmox() class
#

class proxmox() {
	# Check if proxmox is installed on this server
	if( $pve != 'false' )
	{
		# Verify environment
		include proxmox::functions
		include proxmox::requirements

	} else {
		notify{"[ERROR]: This is not a proxmox server":}
		crit("[ERROR]: This is not a proxmox server")
	}
}

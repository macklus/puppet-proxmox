
class proxmox::functions
{
}

define proxmox::functions::line($file, $ensure = 'present') {
	case $ensure {
		default: { err ( "unknown ensure value ${ensure}" ) }
		present: {
			exec { "/bin/echo '${name}' >> '${file}'":
				unless => "/bin/grep -qFx '${name}' '${file}'"
			}
		}
		absent: {
			exec { "/usr/bin/perl -ni -e 'print unless /^\\Q${name}\\E\$/' '${file}'":
				onlyif => "/bin/grep -qFx '${name}' '${file}'"
			}
		}
	}
}

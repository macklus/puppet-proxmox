#
#
#

define proxmox::user($dom='pam',$active=1,$start='',$username='',$usersurname='',$useremail="",$usercomment='') {

    $line = "user:$name@$dom:$active:$start:$username:$usersurname:$useremail:$usercomment:"

    proxmox::functions::line { $line:
        file => "/etc/pve/user.cfg",
        ensure => present
    }
}

#
#
#

define proxmox::group($comment='',$members=[]) {
	$line_members = inline_template("<%= (members).join(',') %>")
	$line = "group:$name:$line_members:$comment:"
	
	proxmox::functions::line { $line:
	    file => "/etc/pve/user.cfg",
	    ensure => present 
	}
}

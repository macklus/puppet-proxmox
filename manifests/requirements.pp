#
#
#

class proxmox::requirements()
{
	#
	# Check system modules
	#
	$modules = [ 
		'arp_tables', 
		'arpt_mangle', 
		'arptable_filter', 
		'ip_queue', 
		'ip_tables', 
		'ipt_recent', 
		'ipt_ah', 
		'ipt_addrtype', 
		'ipt_LOG', 
		'ipt_ULOG', 
		'ipt_MASQUERADE', 
		'ipt_CLUSTERIP', 
		'ipt_NETMAP',
		'ipt_ecn', 
		'ipt_REDIRECT', 
		'ipt_ECN', 
		'ipt_REJECT', 
		'iptable_nat', 
		'iptable_raw', 
		'iptable_mangle', 
		'iptable_filter',
		'nf_nat', 
		'nf_nat_irc', 
		'nf_nat_proto_sctp', 
		'nf_nat_proto_udplite', 
		'nf_nat_amanda', 
		'nf_nat_pptp', 
		'nf_nat_sip', 
		'nf_nat_ftp', 
		'nf_nat_proto_dccp', 
		'nf_nat_snmp_basic',
		'nf_nat_h323', 
		'nf_nat_proto_gre', 
		'nf_nat_tftp', 
		'nf_conntrack_ipv4', 
		'nf_defrag_ipv4',
		'xt_state', 
		'xt_owner', 
		'xt_connlimit', 
		'ip6_tables', 
		'ip6table_filter',
		'ip6table_mangle',
		'ip6t_REJECT',
		'nf_conntrack_ipv6',
	]
	proxmox::functions::line { $modules:
	    file => "/etc/modules",
	    ensure => present 
	}

	#
	# Check vz.conf config file
	#
	file { '/etc/vz/vz.conf':
		ensure  => 'present',
		content => template("proxmox/vz.conf.erb"),
	}		
	
}
